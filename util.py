import pandas as pd
import matplotlib.pyplot as plt
from functools import wraps
import datetime as dt


def decorater(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - start)
        print(f"Ran {func.__name__} shape = {result.shape} took {time_taken}s")
        return result

    return wrapper


@decorater
def start_pipeline(dataf):
    return dataf.copy()


@decorater
def data_audit(dataf, scale=None, plot=False, **kwargs):
    def make_audit(x):
        return pd.Series(
            [
                x.mean(),
                x.std(),
                x.isna().sum() / x.isna().count(),
                x.min(),
                x.quantile(0.01),
                x.quantile(0.05),
                x.quantile(0.1),
                x.quantile(0.25),
                x.quantile(0.5),
                x.quantile(0.75),
                x.quantile(0.9),
                x.quantile(0.95),
                x.quantile(0.99),
                x.max(),
            ],
            index=[
                "Mean",
                "Std",
                "Null Per",
                "Min",
                "0.01",
                "0.05",
                "0.1",
                "0.25",
                "0.5",
                "0.75",
                "0.9",
                "0.75",
                "0.99",
                "Max",
            ],
        )

    if kwargs:
        cond = ""
        for key, values in check.items():
            cond += f"(dataf.{key} == '{values}')"
            cond += " & "

        audit = (
            dataf[eval(cond[:-3])].select_dtypes(["float64", "int64"]).apply(make_audit)
        )

    else:
        audit = dataf.select_dtypes(["float64", "int64"]).apply(make_audit)

    audit = round(audit, 2)

    if scale:
        to_scale = audit.iloc[3:, :]
        to_scale = pd.DataFrame(
            scale.fit_transform(to_scale), columns=audit.columns, index=to_scale.index
        )
        audit = pd.concat([audit.iloc[:3, :], to_scale], axis=0)

    audit = audit.T

    if plot:
        if scale != None:
            audit.iloc[:, 3:].T.plot()
            plt.show()

        else:
            print("\n---  Provide a scaler to plot eg. MinMaxScaler, etc  ---\n")

    return audit


@decorater
def outlier_treatment(
    dataf, lower_quantile=None, upper_quantile=None, IQR=True, std=None
):

    con_dataf = dataf.select_dtypes(["float64", "int64"])
    cat_dataf = dataf[dataf.columns.difference(con_dataf.columns)]

    def IQR_methord(x):
        Q1 = x.quantile(0.25)
        Q3 = x.quantile(0.75)

        IQR = Q3 - Q1
        return x.clip(lower=Q1 - IQR, upper=Q3 + IQR)

    def std_methord(x):
        lower = x.mean() - std * x.std()
        upper = x.mean() + std * x.std()

        return x.clip(lower=lower, upper=upper)

    if lower_quantile or upper_quantile:
        try:
            treated_dataf = con_dataf.apply(
                lambda x: x.clip(lower=lower_quantile, upper=upper_quantile)
            )
        except:
            print(
                "\n---  Check both the bound 'lower_quantile' and 'upper_quantile'  ---\n"
            )
        treated_dataf = pd.concat([treated_dataf, cat_dataf], axis=1)
        return treated_dataf

    if std:
        try:
            treated_dataf = con_dataf.apply(std_methord)
        except:
            print("\n---  Check 'std'! This has to be an Int  ---\n")
        treated_dataf = pd.concat([treated_dataf, cat_dataf], axis=1)
        return treated_dataf

    if IQR == True:
        treated_dataf = con_dataf.apply(lambda x: IQR_methord(x))
        treated_dataf = pd.concat([treated_dataf, cat_dataf], axis=1)
        return treated_dataf


@decorater
def set_dtype(dataf, Int=None, Float=None, Date=None, Object=None):
    if Int:
        for each_feature in Int:
            exec(f"dataf.{each_feature} = dataf.{each_feature}.astype('int64')")

    if Float:
        for each_feature in Float:
            exec(f"dataf.{each_feature} = dataf.{each_feature}.astype('float64')")

    if Date:
        for each_feature in Date:
            try:
                exec(f"dataf.{each_feature} = pd.to_datetime(dataf.{each_feature})")
            except:
                print("-" * 30 + '   Problem in "' + each_feature + '"  ' + "-" * 23)
                exec(f"dataf.{each_feature} = pd.to_datetime(dataf.{each_feature})")

    if Object:
        for each_feature in Float:
            exec(f"dataf.{each_feature} = dataf.{each_feature}.astype('object')")

    return dataf


@decorater
def pipline_summary(
    train_X,
    train_y,
    transformers,
    models,
    test_X=None,
    test_y=None,
    transformers_param_grid=None,
    models_param_grid=None,
    plot=True,
    debug=False,
    **grid_search_params,
):
    try:
        summary = pd.DataFrame()
        for each_t in transformers:
            for each_m in models:
                if transformers_param_grid:
                    each_parma_t = transformers_param_grid[each_t.__class__.__name__]
                    param_grid = dict(
                        zip(
                            [
                                "transformer__" + i
                                for i in each_parma_t.__class__.__name__.keys()
                            ],
                            each_parma_t.values(),
                        )
                    )

                if models_param_grid:
                    each_parma_m = models_param_grid[each_m]
                    try:
                        param_grid.update(
                            dict(
                                zip(
                                    [
                                        "model__" + i
                                        for i in each_parma_m.__class__.__name__.keys()
                                    ],
                                    each_parma_m.values(),
                                )
                            )
                        )

                    except:
                        param_grid = dict(
                            zip(
                                ["model__" + i for i in each_parma_m.keys()],
                                each_parma_m.values(),
                            )
                        )

                pipe = Pipeline([("transformer", each_t), ("model", each_m)])

                if debug:
                    print(pipe)

                if transformers_param_grid or models_param_grid:
                    search = GridSearchCV(
                        pipe, param_grid, n_jobs=-1, **grid_search_params
                    )
                    try:
                        search.fit(train_X, train_y)

                        comb_summary = pd.DataFrame(search.cv_results_)[
                            ["param_" + i for i in param_grid.keys()]
                            + ["mean_test_score"]
                        ]
                        comb_summary.columns = [
                            "param_" + i for i in param_grid.keys()
                        ] + ["test_score"]
                        comb_summary.insert(
                            0, "transformer", value=each_t.__class__.__name__
                        )
                        comb_summary.insert(0, "model", value=each_m.__class__.__name__)
                        summary = pd.concat([summary, comb_summary])

                    except:
                        print(f"Problem! Skipping this, Check debug")

                else:
                    if (
                        test_X.__class__.__name__ != "DataFrame"
                        and test_X.__class__.__name__ != "Series"
                    ) or (
                        test_y.__class__.__name__ != "DataFrame"
                        and test_y.__class__.__name__ != "Series"
                    ):
                        raise Exception("Provide test set both X and y to score.")

                    pipe.fit(train_X, train_y)
                    comb_summary = pd.DataFrame(
                        [pipe.score(test_X, test_y)], columns=["test_score"]
                    )
                    comb_summary.insert(
                        0, "transformer", value=each_t.__class__.__name__
                    )
                    comb_summary.insert(0, "model", value=each_m.__class__.__name__)
                    summary = pd.concat([summary, comb_summary])

        first = [i for i in summary.columns if summary[i].isna().sum() == 0]
        summary = summary[first + list(summary.columns.difference(first))]
        summary.sort_values("test_score", ascending=False)

        if plot:
            summary_hiplot = hiplot.Experiment.from_dataframe(summary)
            summary_hiplot.display()

        return summary

    except:
        print(
            """ Check if these dependencies are satisfied 
        pandas
        sklearn.pipeline.Pipeline
        sklearn.model_selection.GridSearchCV
        hiplot"""
        )


@decorater
def somersD(estimator, dataf, target, verbose=False):
    somersd_df = pd.DataFrame()
    if target.__class__.__name__ == "Series":
        dataf = pd.concat([dataf, target], axis=1)
        target = target.name
    if type(target) != str and target.__class__.__name__ != "Series":
        raise Exception("Target has to be either String or a Series")

    for each_feature in dataf.columns.difference([target]):

        if verbose:
            print(each_feature)

        estimator.fit(pd.DataFrame(dataf[each_feature]), dataf[target])
        y_score = pd.DataFrame(estimator.predict(pd.DataFrame(dataf[each_feature])))

        y_score.columns = ["Score"]

        somers_d = 2 * metrics.roc_auc_score(dataf[target], y_score) - 1

        somersd_obs = pd.DataFrame([each_feature, somers_d]).T
        somersd_obs.columns = ["Variable Name", "SomersD"]

        somersd_df = pd.concat([somersd_df, somersd_obs], axis=0)

    somersd_df = somersd_df.reset_index()
    somersd_df = somersd_df.drop("index", axis=1)
    somersd_df = somersd_df.sort_values("SomersD", ascending=False)

    return somersd_df


@decorater
def cleaning_string(dataf, string_column=None, custom_char=None):

    special_char = r"[~`!@#$%^&*()\-_+={}\]\[|\\/:;,\"'<>?.]"

    if string_column:
        for each_feature in string_column:
            dataf[each_feature] = dataf[each_feature].str.replace(special_char, " ")

    else:
        for each_feature in dataf.select_dtypes(["object"]).columns:
            dataf[each_feature] = dataf[each_feature].str.replace(special_char, " ")

    if custom_char:
        for each_custom in custom_char:
            if string_column:
                for each_feature in string_column:
                    dataf[each_feature] = dataf[each_feature].str.replace(
                        each_custom, " "
                    )
            else:
                for each_feature in dataf.select_dtypes(["object"]).columns:
                    dataf[each_feature] = dataf[each_feature].str.replace(
                        each_custom, " "
                    )

    for each_feature in dataf.select_dtypes(["object"]).columns:
        dataf[each_feature] = dataf[each_feature].str.replace(r" +", " ")
        dataf[each_feature] = dataf[each_feature].str.lower()

    return dataf
